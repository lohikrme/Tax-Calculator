export default class Tax {
    /**
     * Solve total_price by adding tax to list price
     * @param {number} list_price before tax
     * @param {*} tax percent of tax
     * @returns {number} after tax (total_price)
     */
    static applyTax(list_price, tax) {
        const tax_rate = tax / 100;
        const after_tax = list_price + list_price * tax_rate;
        return after_tax;
    }
    /**
     * Solve list_price by reducing tax from total price
     * @param {number} total_price 
     * @param {number} tax 
     * @returns  {number} list price
     */
    static deductTax(total_price, tax) {
        const tax_rate = 1 + (tax / 100);
        const list_price = total_price / tax_rate;
        return list_price;
    }
    /**
     * Solve Tax percent by dividing subtraction of total_price from list_price
     * @param {number} list_price 
     * @param {number} total_price 
     * @returns {number} tax_rate as percent
     */
    static figureTaxRate(list_price, total_price) {
        const tax_rate = (1 / (list_price / total_price)) - 1;
        const tax_rate_percent = tax_rate * 100;
        return tax_rate_percent;
    }
}